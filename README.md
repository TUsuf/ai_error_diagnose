# How to run 🏃‍♂️

```bash
python detect_language.py 
```


# Text preprocessing notebook


```bash
https://www.kaggle.com/sudalairajkumar/getting-started-with-text-preprocessing
```