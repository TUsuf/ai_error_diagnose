import fasttext
from pycountry import languages

# loading the model
model = fasttext.load_model("lid.176.bin")

# give sentence inside a list
sentence = ['je mange de la nourriture']

# detect language
p = model.predict(sentence)

# convert the language ISO code into language name
lang_name = languages.get(alpha_2=p[0][0][0].split("__")[-1]).name


print(lang_name)